package examen_ma�ana;

import java.util.ArrayList;
import java.util.concurrent.Callable;
public class Aquari implements Callable<Boolean>{
	
		
		
		ArrayList<Peix> pecera = new ArrayList<Peix>();
		
		ArrayList<Peix> controlador_pecera = new ArrayList<Peix>();
		
		
		boolean acabar = true;
		boolean lanzados = false;

		
		Object sacar_pez = new Object();
		Object sinc_object = new Object();
		

		
		
		public Aquari() {
			for (int i = 0; i < 20; i++) {
				controlador_pecera.add(new Peix());
			}
			pecera.add(controlador_pecera.remove(0));
		}
		
		
		public Peix ComerPez() {
			synchronized (sinc_object) {
				if(pecera.size()>0) {
					return pecera.remove(0);
				}
			
				return null;
			}
		}
		
	
		@Override
		public Boolean call()  {
			while(acabar) {
				// se espera
				try 
				{
					Thread.sleep(75);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				
				
				
				if(controlador_pecera.size()>0 || pecera.size() > 0) { // notifico que a lanzado el pez
					
					if(controlador_pecera.size()>0) {
						pecera.add(controlador_pecera.remove(0));
					}
					else {
						lanzados = true;
					}
					
					
					synchronized (sacar_pez) {
						sacar_pez.notifyAll();
					}
				}
				
				
				if(controlador_pecera.size()<=0 && pecera.size()<=0 && lanzados ) {
					acabar = false;
					
					return true;
				}
				
				
				
			}
			
			return false;
		}
		
		
		
	
	
}
