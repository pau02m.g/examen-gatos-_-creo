package examen_ma�ana;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
	
		ExecutorService executor = Executors.newCachedThreadPool();
		
		Aquari a = new Aquari();
		
		ArrayList<Future<Boolean>> futuros = new ArrayList<Future<Boolean>>();
		
		ArrayList<Gat> gatetes = new ArrayList<Gat>();
		
		for (int i = 0; i < 5; i++) {
			Gat gato = new Gat(a, "alejandro _ "+i);
			gatetes.add(gato);
			executor.submit(gato);
		}
		
		futuros.add(executor.submit(a));
		
		executor.shutdown();
		
		for (Future<Boolean> futuro : futuros) {
			try {
				futuro.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		// cuando todos acaban
		
		executor.shutdownNow();
		try {
			executor.awaitTermination(1, TimeUnit.SECONDS);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		for(Gat g : gatetes) {
			System.out.println(g.toString());
		}
		
		
		
		
		
		
		
	}
}
